﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Xml.Linq;

namespace CognitiveServicesBot.Services
{
	public static class TranslatorService
	{
		static string _accessToken;

		public static async Task<string> TranslateText(string inputText, string language)
		{
			string url = "http://api.microsofttranslator.com/v2/Http.svc/Translate";
			string query = $"?text={System.Net.WebUtility.UrlEncode(inputText)}&to={language}&contentType=text/plain";

			using (var client = new HttpClient())
			{
				var accessToken = await GetAccessToken();
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
				var response = await client.GetAsync(url + query);
				var result = await response.Content.ReadAsStringAsync();

				if (!response.IsSuccessStatusCode)
					return "Hata: " + result;

				var translatedText = XElement.Parse(result).Value;
				return translatedText;
			}
		}

		static async Task<string> GetAccessToken()
		{
			return _accessToken ?? (_accessToken = await GetAuthenticationToken());
		}

		static async Task<string> GetAuthenticationToken()
		{
			string endpoint = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken";

			var key = WebConfigurationManager.AppSettings["TranslateApiKey"];

			using (var client = new HttpClient())
			{
				client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", key);
				var response = await client.PostAsync(endpoint, null);
				var token = await response.Content.ReadAsStringAsync();
				return token;
			}
		}
	}
}